class Quarto:
    def __init__(self):
        self.board = [
            ["00", "01", "02", "03"],
            ["10", "11", "12", "13"],
            ["20", "21", "22", "23"],
            ["30", "31", "32", "33"]
        ]
        # reprendre en mode liste et index+1 
        self.available_pieces = {"1": Piece("0", "0", "0", "0"),
                                 "2": Piece("0", "0", "0", "1"),
                                 "3": Piece("0", "0", "1", "0"),
                                 "4": Piece("0", "1", "0", "0"),
                                 "5": Piece("1", "0", "0", "0"),
                                 "6": Piece("0", "0", "1", "1"),
                                 "7": Piece("0", "1", "0", "1"),
                                 "8": Piece("0", "1", "1", "0"),
                                 "9": Piece("0", "1", "1", "1"),
                                 "10": Piece("1", "0", "0", "1"),
                                 "11": Piece("1", "0", "1", "0"),
                                 "12": Piece("1", "0", "1", "1"),
                                 "13": Piece("1", "1", "0", "0"),
                                 "14": Piece("1", "1", "0", "1"),
                                 "15": Piece("1", "1", "1", "0"),
                                 "16": Piece("1", "1", "1", "1")
                                 }

    def draw_board(self):

        for i in range(len(self.board)):
            ligne = ""
            for x in range(len(self.board)):
                if len(self.board[i][x]) > 2:
                    ligne += self.convert_code_to_piece(
                        self.board[i][x]) + "   "
                else:
                    ligne += self.board[i][x] + "   "
            print(ligne)

    def draw_available_pieces(self):
        pieces = ""
        for key, value in self.available_pieces.items():
            if int(key) < 10:
                pieces += "  " + value.code_piece() + "  "
            else:
                pieces += "  " + value.code_piece() + "   "
        print(list(self.available_pieces.keys()), "\n", pieces)


# Vérifie si la position x entrée par le joueur est disponible / valide sur le board

    def check_board(self, position):
        return not (len(position) == 2 and (int(position[0]) > 3 or int(position[1]) > 3) and position == self.board[int(position[0])][int(position[1])])


# A partir de 1 car chaque élément commence par un espace cf play_piece: " " + str(piece)

    def convert_code_to_piece(self, code):
        shape = "o" if code[1] == "0" else "x"
        shape = shape.upper() if code[2] == "1" else shape
        color = "31" if code[3] == "0" else "34"
        back = "47" if code[4] == "0" else "40"

        return " " + "\033[1;" + color + ";" + back + "m" + shape + "\033[0m"


# Fonction qui place la selected_piece sur le board

    def play_piece(self, position, piece):
        self.board[int(position[0])][int(position[1])] = " " + str(piece)

    def turn_board(self):
        new_board = []
        test = zip(self.board[0], self.board[1], self.board[2], self.board[3])
        for eachtuple in test:
            new_board.append(eachtuple)
        return new_board


# Fonction qui verifie si combinaisons gagnantes en horizontal et vertical

    def check_horizontal_and_vertical(self, board):
        for line in board:
            for i in range(len(board)):
                if len(line[0]) > 2 and len(line[1]) > 2 and len(line[2]) > 2 and len(line[3]) > 2:
                    if line[0][i] == line[1][i] and line[0][i] == line[2][i] and line[0][i] == line[3][i]:
                        return True
        return False


# idem avec les diagonales

    def check_diagonals(self):
        if len(self.board[0][0]) > 2 and len(self.board[1][1]) > 2 and len(self.board[2][2]) > 2 and len(self.board[3][3]) > 2:
            for i in range(len(self.board)):
                if self.board[0][0][i] == self.board[1][1][i] and self.board[0][0][i] == self.board[2][2][i] and self.board[0][0][i] == self.board[3][3][i]:
                    return True
        elif len(self.board[0][3]) > 2 and len(self.board[1][2]) > 2 and len(self.board[2][1]) > 2 and len(self.board[3][0]) > 2:
            for i in range(len(self.board)):
                if self.board[0][3][i+1] == self.board[1][2][i+1] and self.board[0][3][i+1] == self.board[2][1][i+1] and self.board[0][3][i+1] == self.board[3][0][i+1]:
                    return True
        return False


# fonction verif si partie fini après qu'un joueur est placé une piece : soit plus de pièce à jouer soit combinaison gagnante

    def is_finish(self):
        return len(self.available_pieces) == 0 or self.check_horizontal_and_vertical(self.board) or self.check_diagonals() or self.check_horizontal_and_vertical(self.turn_board())


# 4 caractéristiques possibles avec curses? X = 1 / O = 0, Upper = 1/Lower = 0, Bleu = 1/Rouge = 0, Fond Noir = 1/Blanc = 0


class Piece():
    def __init__(self, shape, upper, color, background_color):
        self.shape = shape
        self.upper = upper
        self.color = color
        self.background_color = background_color


# fonction str de la piece en fonction de ses 4 caractéristiques


    def __str__(self):
        return self.shape + self.upper + self.color + self.background_color

# Renvoie le code qui synthétise les caractéristiques de la piece pour une comparaison plus rapide dans check

    def code_piece(self):
        shape = "o" if self.shape == "0" else "x"
        shape = shape.upper() if self.upper == "1" else shape
        color = "31" if self.color == "0" else "34"
        back = "47" if self.background_color == "0" else "40"

        return "\033[1;" + color + ";" + back + "m" + shape + "\033[0m"


# Boucle du jeu qui sera arrêter si plus de pièces à jouer ou si combinaison gagnante / pour l'instant juste si plus de pièces à jouer
# Cas de figure par défaut:  player 1 commence à jouer en choisissant une pièce pour le player 2 /


def game():
    # Variable qui contiendra à chaque tour l'index et la pièce choisi pour être jouée
    selected_key = ""


# Variable qui contiendra la position sélectionnée par le player
    selected_position = ""

# Variable qui contient le joueur en cours / Mettre en cycle
    ongoing_player = ""

    # Variable qui comptabilise les tours
    ongoing_turn = 0

    quarto = Quarto()
    quarto.draw_board()

    while quarto.is_finish() == False:
        if ongoing_turn % 2 == 0:
            ongoing_player = "Player 2"

        else:
            ongoing_player = "Player 1"
        quarto.draw_available_pieces()
        print("Select a piece to give to " + ongoing_player)
        selected_key = input()

        while selected_key not in list(quarto.available_pieces.keys()):
            print("Piece not available. Please select one of the list")
            quarto.draw_available_pieces()
            selected_key = input()

        selected_piece = quarto.available_pieces[selected_key]
        del quarto.available_pieces[selected_key]

        print(ongoing_player + ": Enter a position for your piece => " +
              selected_piece.code_piece())
        selected_position = input()
        while quarto.check_board(selected_position) is False:
            print("Position not available. Please enter an other one: ")
            selected_position = input()

        quarto.play_piece(selected_position, selected_piece)
        quarto.draw_board()
        ongoing_turn += 1

    print(ongoing_player + ": You win!!!!")


game()
