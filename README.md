# Mini Project


***


## Name
Personal Mini-Projects :

1- Astar

2- ChessTournamentManagement

3- Quarto

4- Tictactoe


## Description
In this repo you will find four mini-projects I created:

- Astar : It's an introduction to pathfinding algorithm by using A*. The goal is to find the fastest path between 2 points with the "A star"  algorithm in order to automate a Zombies Vs Players game.

- ChessTournamentManagement : This project allows the creation and management of a Chess Tournament based on Swiss System. 

- Quarto : The goal of this project is to reproduce the famous (or not) Quarto game on CLI. To learn more about this game you can discover the rules here : https://www.regledujeu.fr/quarto/ or play it directly online : http://quarto.freehostia.com/fr/ . 

- tictactoe : This final project reproduce the Tic Tac Toe game, Player VS Machine on CLI. Have Fun !



## Installation
You will need to have python install (version 3.8 >)
For the ChestTournamentManagement project, you'll also need to install TinyDB.

```bash
pip install tinydb
```





## Author
Randrianirina Manon



