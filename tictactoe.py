import random

class Game:
    def __init__(self, player = "Player"):
        self.grid = [" " for i in range(9)]
        self.player = player
        self.valid = [str(i) for i in range(1, 10)] #available placements
        self.tourCounter = 0


    def display_grid_with_number(self):
        interline = "--+---+--"
        spaces = " "*10
        print(f"\n{spaces}1 | 2 | 3 \n{spaces}{interline} \n{spaces}4 | 5 | 6 \n{spaces}{interline} \n{spaces}7 | 8 | 9\n")

    def display_grid(self):
        interline = "--+---+--"
        spaces = " "*10
        print(f"\n{spaces}{self.grid[0]} | {self.grid[1]} | {self.grid[2]} \n{spaces}{interline} \n{spaces}{self.grid[3]} | {self.grid[4]} | {self.grid[5]} \n{spaces}{interline} \n{spaces}{self.grid[6]} | {self.grid[7]} | {self.grid[8]}\n")
   
    
    def check_rows(self, nb, empty_space):
        result = []
        for i in range(0 , len(self.grid),3):
            if (self.grid[i:i+3].count("O") == nb or self.grid[i:i+3].count("X") == nb) and  self.grid[i:i+3].count(" ") == empty_space:
                result.append(True)
            else:
                result.append(False)
        return result
        

    def check_columns(self, nb, empty_space):
        new_grid = list(zip(self.grid[0:3], self.grid[3:6], self.grid[6:9]))
        result = []
        for t in range(len(new_grid)):
            if new_grid[t].count(" ") == empty_space and (new_grid[t].count("O") == nb or new_grid[t].count("X") == nb):
                result.append(True)
            else:
                result.append(False)
        return result        

    def check_diagonals(self, nb, empty_space):
        result = []
        if (self.grid[0:9:4].count(" ") == empty_space and (self.grid[0:9:4].count("X") == nb or self.grid[0:9:4].count("O") == nb))  or (self.grid[2:7:2].count(" ") == empty_space and (self.grid[2:7:2].count("X") == nb or self.grid[2:7:2].count("O") == nb)):
            result.append(True)
        else:
            result.append(False)
        return result


        
    def check_if_win(self):
        return any(self.check_columns(3, 0) + self.check_rows(3, 0) + self.check_diagonals(3, 0))
    

    def computer_play(self):
        
        if self.tourCounter == 2: #First tour
            self.grid[int(random.choice(self.valid)) - 1] = "O"
            
            
        elif  True in self.check_rows(2, 1):
            for i in range(0 , len(self.grid),3):
                if self.grid[i:i+3].count(" ") == 1 and (self.grid[i:i+3].count("X") == 2 or self.grid[i:i+3].count("O") == 2):
                    self.grid[i:i+3] = ["O" if v == " " else v for v in self.grid[i:i+3]]
                    break

        elif True in self.check_columns(2, 1): 
            result = list(zip(self.grid[0:3], self.grid[3:6], self.grid[6:9]))
            for t in range(len(result)):
                if  result[t].count(" ") == 1 and (result[t].count("O") == 2 or result[t].count("X") == 2):
                    result[t] = ["O" if i == " " else i for i in result[t]]
                    break
                    
            result = list(zip(result[0], result[1], result[2]))
            self.grid = [el for i in result for el in i] 
           
        elif True in self.check_diagonals(2, 1) :
            if (self.grid[0:9:4].count("X") == 2 or self.grid[0:9:4].count("O") == 2) and self.grid[0:9:4].count(" ") == 1:
                self.grid[0:9:4] = ["O" if i == " " else i for i in self.grid[0:9:4]]
                
            else:
                self.grid[2:7:2]= ["O" if i == " " else i for i in self.grid[2:7:2]]
                
        else:
            for i in range(len(self.grid)):
                if self.grid[i] == " ":
                    self.grid[i] = "O"
                    break
        
    def play(self): 
        self.player = input("Please enter your name => ")
        self.display_grid_with_number()  
        while( not self.check_if_win() and bool(self.valid)): # ou grid pleine !!!
            self.tourCounter += 1
            self.display_grid()
            if self.tourCounter % 2 != 0:
                placement = ""
                while(placement not in self.valid): 
                    placement = input(f"Choose your placement (available placements {self.valid}) =>  ")
                self.valid.remove(placement)
                self.grid[int(placement)-1] = "X"
                last_player = self.player
            else:
                print("Computer played :")
                self.computer_play()
                self.valid = [str(i+1) for i in range(len(self.grid)) if self.grid[i] == " "]
                last_player = "Computer" 
        if self.tourCounter == 9:
            self.display_grid()
            print("Tied game.. Try again !")
        else:
            self.display_grid()
            print(f"{last_player} win !!!!")


if __name__ == "__main__":  
    tictactoe = Game()
    tictactoe.play()


