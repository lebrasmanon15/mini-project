import random


class Quarto:
    def __init__(self):
        self.board = [
            ["00", "01", "02", "03"],
            ["10", "11", "12", "13"],
            ["20", "21", "22", "23"],
            ["30", "31", "32", "33"]
        ]

        self.available_pieces = [Piece("0", "0", "0", "0"),
                                 Piece("0", "0", "0", "1"),
                                 Piece("0", "0", "1", "0"),
                                 Piece("0", "1", "0", "0"),
                                 Piece("1", "0", "0", "0"),
                                 Piece("0", "0", "1", "1"),
                                 Piece("0", "1", "0", "1"),
                                 Piece("0", "1", "1", "0"),
                                 Piece("0", "1", "1", "1"),
                                 Piece("1", "0", "0", "1"),
                                 Piece("1", "0", "1", "0"),
                                 Piece("1", "0", "1", "1"),
                                 Piece("1", "1", "0", "0"),
                                 Piece("1", "1", "0", "1"),
                                 Piece("1", "1", "1", "0"),
                                 Piece("1", "1", "1", "1")
                                 ]
        self.positions_list = ["00", "01", "02", "03", "10", "11",
                               "12", "13", "20", "21", "22", "23", "30", "31", "32", "33"]

    def draw_board(self):

        for i in range(len(self.board)):
            ligne = ""
            for x in range(len(self.board)):
                if len(self.board[i][x]) > 2:
                    ligne += self.convert_code_to_piece(
                        self.board[i][x]) + "   "
                else:
                    ligne += self.board[i][x] + "   "
            print(ligne)

    def draw_available_pieces(self):
        pieces = ""
        for i in range(len(self.available_pieces)):
            if i < 9:
                pieces += self.available_pieces[i].code_piece() + "  "
            else:
                pieces += " " + self.available_pieces[i].code_piece() + "  "
        print(list(range(1, len(self.available_pieces)+1)), "\n", pieces)


# Vérifie si la position x entrée par le joueur est disponible / valide sur le board


    def check_board(self, position):
        return (len(position) == 2 and (int(position[0]) < 4 or int(position[1]) < 4) and position == self.board[int(position[0])][int(position[1])])


# A partir de 1 car chaque élément commence par un espace cf play_piece: " " + str(piece)


    def convert_code_to_piece(self, code):
        shape = "o" if code[1] == "0" else "x"
        shape = shape.upper() if code[2] == "1" else shape
        color = "31" if code[3] == "0" else "34"
        back = "47" if code[4] == "0" else "40"

        return " " + "\033[1;" + color + ";" + back + "m" + shape + "\033[0m"

    def play_piece(self, position, piece):
        self.board[int(position[0])][int(position[1])] = " " + str(piece)

    def turn_board(self):
        new_board = []
        test = zip(self.board[0], self.board[1], self.board[2], self.board[3])
        for eachtuple in test:
            new_board.append(eachtuple)
        return new_board


# Fonction qui verifie si combinaisons gagnantes en horizontal et vertical

    def check_horizontal_and_vertical(self, board):
        for line in board:
            for i in range(len(board)):
                # modifier avec all()
                if len(line[0]) > 2 and len(line[1]) > 2 and len(line[2]) > 2 and len(line[3]) > 2:
                    if line[0].strip()[i] == line[1].strip()[i] and line[0].strip()[i] == line[2].strip()[i] and line[0].strip()[i] == line[3].strip()[i]:
                        return True
        return False


# idem avec les diagonales


    def check_diagonals(self):
        if len(self.board[0][0]) > 2 and len(self.board[1][1]) > 2 and len(self.board[2][2]) > 2 and len(self.board[3][3]) > 2:
            for i in range(len(self.board)):
                if self.board[0][0].strip()[i] == self.board[1][1].strip()[i] and self.board[0][0].strip()[i] == self.board[2][2].strip()[i] and self.board[0][0].strip()[i] == self.board[3][3].strip()[i]:
                    return True
        elif len(self.board[0][3]) > 2 and len(self.board[1][2]) > 2 and len(self.board[2][1]) > 2 and len(self.board[3][0]) > 2:
            for i in range(len(self.board)):
                if self.board[0][3].strip()[i] == self.board[1][2].strip()[i] and self.board[0][3].strip()[i] == self.board[2][1].strip()[i] and self.board[0][3].strip()[i] == self.board[3][0].strip()[i]:
                    return True
        return False


# fonction verif si partie fini après qu'un joueur est placé une piece : soit plus de pièce à jouer soit combinaison gagnante

    def is_finish(self):
        return len(self.available_pieces) == 0 or self.check_horizontal_and_vertical(self.board) or self.check_diagonals() or self.check_horizontal_and_vertical(self.turn_board())


# Partie IA

    def ia_play_piece(self, selected_piece):
        if self.has_possible_winning_move(selected_piece) != False:
            return self.has_possible_winning_move(selected_piece)
        else:
            selected_position = random.choice(self.positions_list)
            while self.check_board(selected_position) == False:
                selected_position = random.choice(self.positions_list)
            return selected_position

    def choose_piece_for_player(self):
        for i in range(len(self.available_pieces)):
            selected_piece = random.choice(self.available_pieces)
            if self.has_possible_winning_move(str(selected_piece).strip()) == False:
                self.available_pieces.remove(selected_piece)
                return selected_piece
        self.available_pieces.remove(selected_piece)
        return selected_piece

    def has_possible_winning_move(self, selected_piece):
        return self.can_win_vertical_horizontal(self.board, selected_piece) or self.can_win_vertical_horizontal(self.turn_board(), selected_piece) or self.can_win_diagonal(selected_piece)

# Pour chaque ligne de la board on regarde s'il y a une pièce = 1 ou un emplacement vide = 0 puis on regarde les lignes dans lesquelles il y a 3 pièces et on récupère
# leur index pour identifier la ligne où il y a le cas de figure 3 pièces + 1 emplacement vide

# renvoie une liste avec l'index de la ligne où se trouve 3 pièces

    def has_three_pieces_vertical_horizontal(self, board):
        liste = []
        for line in board:
            liste.append([1 if len(i.strip()) == 4 else 0 for i in line])
        return [i if sum(liste[i]) == 3 else False for i in range(len(liste))]

# renvoie une liste avec la diagonale où se trouve 3 pièces

    def has_three_pieces_diagonal(self):
        if sum([1 if len(self.board[i][i].strip()) == 4 else 0 for i in range(len(self.board))]) == 3:
            return [self.board[i][i].strip() for i in range(len(self.board))]
        elif sum([1 if len(self.board[i][3-i].strip()) == 4 else 0 for i in range(len(self.board))]) == 3:
            return [self.board[i][3-i].strip() for i in range(len(self.board))]
        return False


# fonction qui prend une liste de 4 éléments et qui vérifie si ces éléments forment une combinaison gagnante

    def winning_four(self, liste):
        return True in [True for i in range(len(liste)) if liste[0][i] == liste[1][i] and liste[0][i] == liste[2][i] and liste[0][i] == liste[3][i]]

    def can_win_vertical_horizontal(self, board, selected_piece):
        for i in self.has_three_pieces_vertical_horizontal(board):
            if type(i) is int:
                win_position = [x for x in board[i] if len(x) == 2]
                if self.winning_four([selected_piece if len(x) == 2 else x.strip() for x in board[i]]):
                    return win_position[0]
        return False

    def can_win_diagonal(self, selected_piece):
        if self.has_three_pieces_diagonal():
            win_position = [
                x for x in self.has_three_pieces_diagonal() if len(x) == 2]
            if self.winning_four([selected_piece if len(x) == 2 else x.strip() for x in self.has_three_pieces_diagonal()]):
                return win_position[0]
        return False


# 4 caractéristiques  X = 1 / O = 0, Upper = 1/Lower = 0, Bleu = 1/Rouge = 0, Fond Noir = 1/Blanc = 0


class Piece():
    def __init__(self, shape, upper, color, background_color):
        self.shape = shape
        self.upper = upper
        self.color = color
        self.background_color = background_color


# fonction str de la piece en fonction de ses 4 caractéristiques

    def __str__(self):
        return self.shape + self.upper + self.color + self.background_color

# Renvoie le code qui synthétise les caractéristiques de la piece pour une comparaison plus rapide dans check

    def code_piece(self):
        shape = "o" if self.shape == "0" else "x"
        shape = shape.upper() if self.upper == "1" else shape
        color = "31" if self.color == "0" else "34"
        back = "47" if self.background_color == "0" else "40"

        return "\033[1;" + color + ";" + back + "m" + shape + "\033[0m"


# Boucle du jeu qui sera arrêter si plus de pièces à jouer ou si combinaison gagnante / pour l'instant juste si plus de pièces à jouer
# Cas de figure par défaut:  player 1 commence à jouer en choisissant une pièce pour le player 2 /


def game():
    # Variable qui contiendra à chaque tour l'index et la pièce choisi pour être jouée
    selected_key = ""


# Variable qui contiendra la position sélectionnée par le player
    selected_position = ""

# Variable qui contient le joueur en cours / Mettre en cycle
    ongoing_player = ""

    # Variable qui comptabilise les tours
    ongoing_turn = 0

    quarto = Quarto()
    quarto.draw_board()

    while quarto.is_finish() == False:
        if ongoing_turn % 2 != 0:
            quarto.draw_available_pieces()
            print("Select a piece to give to computer")
            selected_key = int(input())

            while selected_key not in list(range(1, len(quarto.available_pieces)+1)):
                print("Piece not available. Please select one of the list")
                quarto.draw_available_pieces()
                selected_key = input()

            selected_piece = quarto.available_pieces[selected_key - 1]
            del quarto.available_pieces[selected_key-1]
            print("Au tour du computer de jouer la pièce => " +
                  selected_piece.code_piece())
            # Fonction qui place la pièce
            selected_position = quarto.ia_play_piece(
                str(selected_piece).strip())
            quarto.play_piece(selected_position, selected_piece)
            quarto.draw_board()
            ongoing_turn += 1
        else:
            # Fonction pour choisir une pièce à donner au player
            selected_piece = quarto.choose_piece_for_player()
            last_piece = selected_piece
            print(ongoing_player + ": Enter a position for your piece => " +
                  selected_piece.code_piece())
            selected_position = input()
            while quarto.check_board(selected_position) is False:
                print("Position not available. Please enter an other one: ")
                selected_position = input()

            quarto.play_piece(selected_position, selected_piece)
            quarto.draw_board()
            ongoing_turn += 1

    final = "Sorry... Computer win" if ongoing_turn % 2 == 0 else "You win!!!!"
    print(final)


game()
