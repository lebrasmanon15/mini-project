from typing import List as L, Tuple as T
from datetime import datetime


class Player:
    def __init__(self, firstName : str, lastName: str, birthday: str, gender: str, total_point=0) -> None:
        self.firstName = firstName
        self.lastName = lastName
        self.birthday = birthday
        self.gender = gender 
        self.total_point = total_point
        self.rank = 1000000 #valeur par défaut


    def __str__(self) -> str:
        return f"Player N°{self.rank} : {self.lastName} {self.firstName}"

    def serializerPlayer(self):
        serialized_player = {"firstName":self.firstName,
                         "lastName": self.lastName,
                         "birthday": self.birthday,
                         "gender": self.gender,
                         "rank": self.rank,
                         "total_point": self.total_point}
        return serialized_player
                



class Match:
    def __init__(self, player1 : Player, player2 : Player, resultP1 = 0,  resultP2 = 0) -> None:
        self.player1 = player1
        self.resultP1 = resultP1
        self.player2 = player2
        self.resultP2 = resultP2

    def __str__(self):
        return f"{self.player1['firstName']} VS {self.player2['firstName']}"

    def serializerMatch(self):
        serialized_match = {"player1":self.player1,
                         "resultP1": self.resultP1,
                         "player2": self.player2,
                         "resultP2": self.resultP2}
        return serialized_match
    
    

    


class Round:
    def __init__(self, name: str, matchs = []) -> None:

        self.name  = name
        self.begin = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        self.end = None
        self.matchs = matchs

    def __str__(self) -> str:
        return f"{self.name} Start => {self.begin}"
    
    def serializerRound(self):
        serialized_round = {"name":self.name,
                         "begin": self.begin,
                         "end": self.end,
                         "matchs": self.matchs}
        return serialized_round

    
        
        


class Tournament:
    def __init__(self, name : str, place : str, players : L[Player],\
         timeController : str, description : str, rounds = [], nbRound = 4) -> None:
        self.name = name
        self.place = place
        self.date = datetime.now().strftime("%d/%m/%Y")
        self.nbRound = nbRound
        self.rounds = rounds
        self.players = players
        self.timeController = timeController
        self.description = description

    def __str__(self) -> str:
        return f"{self.name} Tournament \n=> {self.date}, {self.place}\nDescription => {self.description} \n" 

    def serializerTournament(self):
        serialized_tournament = {"name":self.name,
                         "place": self.place,
                         "date": self.date,
                         "nbRound": self.nbRound,
                         "rounds": self.rounds,
                         "players": self.players,
                         "timeController" : self.timeController,
                         "description" : self.description}
        return serialized_tournament
    
    

    


    
        