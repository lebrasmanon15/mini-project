from typing import List as L, Tuple as T
from models import *
from tinydb import TinyDB, Query, where
from tinydb.table import Document
from tinydb.operations import add
import json

class Controllers: 

    def __init__(self):
        self.check_db = self.checkJsonFile()
        self.db = TinyDB('db.json')

    #Part 1 main menu
    def createTournament(self)->Tournament:
        name = input("\nTournament's name => ")
        place =input("\nPlace => ")
        print("\n**************** Add or Create 8 players ****************")
        players = []
        for i in range(8):
            choice = input("\nDo you want to create a new player (1) or add one (2) ? (with last name) ")
            while choice not in ["1", "2"]:
                choice = input("Please enter 1 => to create a new player  or 2 => to add one ? ")
            if choice == "1":
                players.append(self.createPlayer().serializerPlayer())
            else:
                player = self.addPlayer() 
                players.append(player)
            print(f"\nPlayer n° {i+1} successfully created")

        timeController = input("\nChoose a time controller between : bullet, blitz or quick shot => ")
        while timeController not in ["bullet", "blitz", "quick shot"]:
            timeController = input("\nPlease choose between : bullet, blitz or quick shot => ")
        description = input("\nDescribe in few words this tournament => ")
        tournament = Tournament(name, place, players, timeController, description)

        print(f"\nTournament {tournament.name} successfully created ! ")
        #New tournament added to the db
        self.db.table("Tournaments").insert(tournament.serializerTournament())
        return tournament
        

    def createPlayer(self)->Player:
        lastName = input("\nLast name ? ")
        firstName = input("\nFirst name ? ")
        birthday = input("\nDate of birth ? (dd/mm/yyyy) ")
        gender = input("\nGender ? (M / F) ")
        while gender not in ["M","F"]:
            gender = input("\nGender ? (M / F) ")
        player = Player(firstName, lastName, birthday, gender)
        self.db.table("Players").insert(player.serializerPlayer())
        return player 
    
    def addPlayer(self)->Player:
        lastName = input("\nWhat is the player's last name ? ")
        players = self.db.table("Players")
        player = players.search(Query()['lastName'] == lastName)
        return dict(player[0])


    def match_exist(self, tournament, player1, player2): 
        
        for round in tournament.rounds:
            for match in round['matchs']:
                if player1 in [match['player1'], match['player2']] and player2 in [match['player1'], match['player2']]:
                    return True
        return False
            



    def swissSystem(self, tournament)->L[Match]:
        new_round = Round(name = f"Round n° {len(tournament.rounds)+1}")
        new_round.matchs = []
        tournament.rounds.append(new_round.serializerRound())
        print(f"Round en création: {new_round.matchs}")
        self.db.table("Tournaments").update(tournament.serializerTournament(), where('name') == tournament.name)
        print(f"Round après update: {tournament.rounds[-1]['matchs']}")
        if len(tournament.rounds) == 1: #If first round
            
            tournament.players.sort(key=lambda x: "total_point", reverse=True)
            first_half = tournament.players[0:int(len(tournament.players)/2)] 
            second_half = tournament.players[int(len(tournament.players)/2):]
            pair_for_new_matchs = list(zip(first_half, second_half))
            for pair in pair_for_new_matchs: 
                tournament.rounds[-1]['matchs'].append(Match(pair[0], pair[1]).serializerMatch())
        else:
            
            players_without_opponent = tournament.players
            print(f"Round en cours : {tournament.rounds[-1]}")
            for player in tournament.players:
                for adversary in players_without_opponent: 
                    
                    if not self.match_exist(tournament, player, adversary)  and player != adversary :
                        print(f"New match {player}, {adversary}")
                        print(tournament.rounds[-1]['name'])
                        tournament.rounds[-1]['matchs'].append(Match(player, adversary).serializerMatch())
                        players_without_opponent.remove(player)
                        players_without_opponent.remove(adversary)
                    else : pass
        
        self.db.table("Tournaments").update(tournament.serializerTournament(), where('name') == tournament.name)
        return tournament.rounds[-1]['matchs']




    def updateRanking(self, tournament_name):
        tournaments_table = self.db.table("Tournaments")
        tournament = dict(tournaments_table.search(Query()["name"] == tournament_name)[0])
        players_by_points = sorted(tournament['players'], key=lambda d: d["total_point"], reverse = True)
        for count, player in enumerate(players_by_points):
            player['rank'] = count+1
            self.db.table("Players").update(player, where('firstName') == player['firstName'])
        
        

    def allPlayersByRank(self)->L[Player]:
        players = self.db.table("Players").all()
        return sorted(players, key=lambda d: d["rank"], reverse = True)
    

    def allPlayersByAlpha(self)->L[Player]:
        players = self.db.table("Players").all()
        return sorted(players, key=lambda d: d["lastName"])

    def allTournament(self)->L[Tournament]:
        tournaments = self.db.table("Tournaments").all()
        return tournaments

    def specificTournamentDetails(self,tournament_name)->Tournament:
        tournament = self.db.table("Tournaments").search(Query()["name"] == tournament_name)
        return dict(tournament[0])


    def checkJsonFile(self):
        try:
            with open("db.json") as fp:
                listdata = json.load(fp)
            pass
        except FileNotFoundError:
            self.db = TinyDB('db.json')
            tournaments = self.db.table("Tournaments")
            players = self.db.table("Players")
        except json.decoder.JSONDecodeError:
            self.db = TinyDB('db.json')
            tournaments = self.db.table("Tournaments")
            players = self.db.table("Players")

    def finishRound(self, round, tournament):
        round['end'] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        for match in round['matchs']:
            match['resultP1'] = int(input(f"\nPlease enter the result of {match['player1']['firstName']} => "))
            match['resultP2'] = int(input(f"\nPlease enter the result of {match['player2']['firstName']} => "))
            print(f"\n{self.displayResult(match)}")
            self.analyzeResult(match, tournament)
        self.db.table("Tournaments").update(tournament.serializerTournament(), where('name') == tournament.name)
    

    def displayResult(self, match):
        return f"{match['player1']['firstName']} : {match['resultP1']} VS {match['player2']['firstName']} : {match['resultP2']}"
    
    def analyzeResult(self, match, tournament):
        if match['resultP1'] > match['resultP2']:
            match['player1']['total_point'] += 1

        elif match['resultP1'] < match['resultP2']:
            match['player2']['total_point'] += 1
        
        else:
            match['player1']['total_point'] += 0.5
            match['player2']['total_point'] += 0.5

        self.db.table("Tournaments").update(tournament.serializerTournament(), where('name') == tournament.name)
            

            








