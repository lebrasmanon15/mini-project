# start_dist = is the distance between the current node and the start node
# heuristic = estimated distance from the current node to the end node
# cost = start_dist + heuristic
HEURISTIC_WALL = 100000000
LINE = 6
COLUMN = 5


class Node:
    def __init__(self,  position, parent=None):

        self.position = position
        self.parent = parent
        self.start_dist = 0
        self.heuristic = 0
        self.cost = self.start_dist + self.heuristic
        self.walls = [(0, 1), (0, 4), (1, 1), (1, 2), (3, 0), (3, 1), (3, 3)]
        # [(1, 3), (2, 3), (2, 4)] cas simple

    def __eq__(self, other):
        return self.position == other

    def __str__(self):

        if self.position == (4, 1):
            return "x"
        elif self.position == (0, 2):
            return "X"
        elif self.position in self.walls:
            return "W"
        else:
            return "O"

    def calculation_heuristic(self, end_node):
        return (((end_node.position[0] - self.position[0])**2) + ((end_node.position[1] - self.position[1])**2))


class Maze:
    def __init__(self, line, column):
        self.line = line
        self.column = column

        grid = []
        for l in range(self.line):
            grid.append([Node((l, c)) for c in range(self.column)])
        self.grid = grid
        self.walls = [(0, 1), (0, 4), (1, 1), (1, 2), (3, 0), (3, 1), (3, 3)]

    def create_grid(self):
        for line in self.grid:
            print([str(node) for node in line])

    def check_neighbours(self, node):
        neighbours = []
        if node.position[0] > 0:  # Peut avoir un voisin en haut
            neighbours.append(
                self.grid[node.position[0] + (- 1)][node.position[1]])
        if node.position[0] < len(self.grid[0]):  # Peut avoir un voisin en bas
            neighbours.append(
                self.grid[node.position[0] + 1][node.position[1]])
        if node.position[1] > 0:  # Peut avoir un voisin à gauche
            neighbours.append(
                self.grid[node.position[0]][node.position[1] + (- 1)])
        # Peut avoir un voisin à droite
        if node.position[1] < 4:
            neighbours.append(
                self.grid[node.position[0]][node.position[1] + 1])

        return neighbours

    def wall_distance(self, node):
        distances = []
        for wall in self.walls:
            distances.append(
                (((wall[0] - node.position[0])**2) + ((wall[1] - node.position[1])**2)))
        return sum(distances)

    def find_path(self, start, end):

        start_node = Node(start)
        end_node = Node(end)

        open_list = []
        closed_list = []
        path = []
        open_list.append(start_node)
        current_node = start_node
        while len(open_list) != 0:
            # prend le node qui a le plus petit cost de la open list
            for node in open_list:
                if node.cost == min([node.cost for node in open_list]):
                    current_node = node

            if current_node == end_node:
                while current_node != start_node:
                    path.append(current_node.parent)
                    current_node = current_node.parent
                return [node.position for node in path[::-1]]

            # récupère la liste des coordonnées des neighbours du current node
            neighbours = self.check_neighbours(current_node)

            # pour chaque voisin on créé un noeud et on calcul ses 3 caractéristiques
            for n in neighbours:  # créer une fonction du genre create_and_calculation_node() ou ça revient au init??
                new_node = Node(n.position, parent=current_node)
                new_node.start_dist = current_node.start_dist + 1
                new_node.heuristic = HEURISTIC_WALL if n in n.walls else new_node.calculation_heuristic(
                    end_node)
                new_node.cost = new_node.start_dist + new_node.heuristic

                # on regarde si notre new node n'existe pas déjà dans la open list ou dans la closed list avec une start_dist moindre sinon on l'ajoute
                if new_node not in open_list and new_node not in closed_list and new_node.position not in self.walls:
                    open_list.append(new_node)
                # new node dans open list et a une meilleure start dist alors je remplace
                elif any([True for node in open_list if new_node == node and new_node.start_dist < node.start_dist]):
                    for index, element in enumerate(open_list):
                        if element == new_node:
                            open_list[index] = new_node
                    if new_node in closed_list:  # si il est dans la closed list je le supprime
                        closed_list.remove(new_node)

            closed_list.append(current_node)
            open_list.remove(current_node)

        print("finish!")


test_maze = Maze(LINE, COLUMN)
test_maze.create_grid()
start = (4, 1)
end = (0, 2)
print(test_maze.find_path(start, end))

#revoir chaque fonction / file
