from models import *
from controllers import *


class Views:

    def __init__(self): #Create a dico to simulate the switch/case
        self.controller = Controllers()


    def mainMenu(self):
        choice = int(input("\n********************* Main Menu *********************\n\n\
            1 => Create a tournament\n\n\
            2 => List of all players\n\n\
            3 => List of all tournaments\n\n\
            4 => Tournament's informations\n\n\
            5 => Quit\n\n\
            Your choice ? "))
        while choice not in list(range(1,6)):
            choice = int(input("Please select 1, 2, 3, 4 or 5 : "))

        if choice == 1:
            tournament = self.controller.createTournament()
            self.manageTournamentMenu(tournament)
        elif choice == 2:
            self.displayAllPlayersMenu()
        elif choice == 3:
            tournaments = self.controller.allTournament()
            print("\n********************* All Tournaments *********************\n")
            for tournament in tournaments:
                print(tournament)
                print("\n----------------------------\n")
        elif choice == 4:
            tournament_name = input("Please enter the tournament's name you are interested in? ")
            specific_tournament = self.controller.specificTournamentDetails(tournament_name)
            print(f"\n{specific_tournament['name']}")
            print(f"\nPlayers => {specific_tournament['players']}\n\nTime controller type => {specific_tournament['timeController']}")
            for count, match in enumerate([round.matchs for round in specific_tournament['rounds']]):
                print(f"\nRound N° {count+1} => {match}")
        else:
            print("\nSee you soon !")
                
        

    def manageTournamentMenu(self, tournament: Tournament):
        print(f"\nTournament {tournament.name} start !")
        for i in range(tournament.nbRound):
            self.controller.db.update(tournament)
            print(f"\n************ Round {i+1} ************\n")
            matchs = self.controller.swissSystem(tournament)
            for count, match in enumerate(matchs):
                print(f"\nMatch N°{count+1} : {match['player1']['firstName']} VS {match['player2']['firstName']}")
            self.tournamentMenu(tournament)
            
                
            

    def tournamentMenu(self, tournament):
        choice = int(input("\n********************* Tournament Menu *********************\n\n\
            1 => Close the round\n\n\
            2 => Display current matchs\n\n\
            Your choice ? "))
        while choice not in list(range(1,3)):
            choice = int(input("Please select 1 or 2 : "))
        if choice == 1:
            self.controller.finishRound(tournament.rounds[-1], tournament)
            self.controller.updateRanking(tournament.name)
        else:
            for count, match in enumerate(tournament.rounds[-1].matchs):
                print(f"\nMatch N°{count+1} : {match}")
            self.tournamentMenu(tournament)
 

    def displayAllPlayersMenu(self):
        choice = int(input("\n********************* Players display Menu *********************\n\n\
            1 => Display by alphabetic order\n\n\
            2 => Display by ranking\n\n\
            3 => Back to main menu\n\n\
            Your choice ? "))
        while choice not in list(range(1,4)):
            choice = int(input("Please select 1, 2 or 3 : "))

        if choice == 1:
            players = self.controller.allPlayersByAlpha()
            print("\n************** All players by alphabetic order **************")
            for player in players:
                print(f"\n{player}")
            self.displayAllPlayersMenu()

        elif choice == 2:
            players = self.controller.allPlayersByRank()
            print("\n************** All players by rank **************")
            for player in players:
                print(f"\n{player}")
            self.displayAllPlayersMenu()
        
        else:
            self.mainMenu()

            




view = Views()
view.mainMenu()


